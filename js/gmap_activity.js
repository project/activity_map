
/**
 * Maps & Activity.
 */
 
gmap_activity_markers = new Array();
gmap_activity_prev_marker = null;

$(document).ready(function(){
	if (!Drupal.gmap){
		return;
	}
	activity_map = Drupal.gmap.getMap('activity_gmap');
	//alert(activity_map);
	if (activity_map){
		gmap_activity_load_data();
		gmap_activity_display();
	}
});

function gmap_activity_load_data(){
	
	$.ajax({
			url: Drupal.settings.basePath + '?q=activity_map/all/json', //if no clear URL?
			type: 'GET',
			dataType: 'json',
			timeout: 5000,
			error: function(){
				setTimeout('gmap_activity_load_data()', 5000); //retry after 5 sec.
			},
			success: function(data){
				// do something with xml
				gmap_activity_process_markers(data);
				setTimeout('gmap_activity_load_data()', 20000);
			}
	});
}

function gmap_activity_display(){
	//alert(1);
  if(gmap_activity_prev_marker){
		//remove previous one.
		gmap_activity_prev_marker.remove();
  }
  //alert(gmap_activity_markers.length);
	if(gmap_activity_markers == null || gmap_activity_markers.length==0){
		setTimeout('gmap_activity_display()', 5000);
		return;
	}
	
	gm = gmap_activity_markers[0];
	//alert(gm.activity);
	point = new GLatLng(gm.latitude, gm.longitude);
	
	//
	
	markerIcon = new GIcon();
	markerIcon.iconSize = new GSize(30, 35);
	markerIcon.shadowSize = new GSize(22, 20);
	markerIcon.iconAnchor = new GPoint(6, 20);
	
	markerOptions = { title : gm.activity  }; //icon : markerIcon, 
	
	//alert(activity_map.map);
	
	gmap_activity_prev_marker = new GMarker(point, markerOptions);
	//GEvent.addListener(gmap_activity_prev_marker, "click", function() {
	activity_map.map.openInfoWindowHtml(point, gm.activity);
	
	activity_map.map.addOverlay(gmap_activity_prev_marker);
	gmap_activity_markers.shift();
	
	setTimeout('gmap_activity_display()', 5000);
	
}

//setInterval('twiter_display()', 5000);
//setInterval('FillTwitters()', 5000*20);

function gmap_activity_process_markers(data){

   if (data){
   	gmap_activity_markers = data;
		//alert(data);
		
   }

}



